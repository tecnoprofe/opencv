import cv2
import numpy as np

#Leer una imagen
imagen = cv2.imread('/var/www/tecnoprofe/opencv/img/upds.jpeg', cv2.IMREAD_UNCHANGED)
print(imagen.shape)

# Asignar a los canales verdes 0
imagen[:,:,1] = np.zeros([imagen.shape[0], imagen.shape[1]])

#Guardar imagen
cv2.imwrite('/var/www/tecnoprofe/opencv/img/upds_sinverde.jpeg',imagen) 

cv2.imshow("Edge Detected Image", imagen) 
#Esperar hasta que se presione una tecla
cv2.waitKey(0) 
# Destruir la ventana
cv2.destroyAllWindows()

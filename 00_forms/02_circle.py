import cv2
import os
myimage=cv2.imread(os.path.dirname(__file__)+'\\..\\img\\book.jpg')
cv2.circle(myimage, (170, 170), 160, (6, 73, 218), 4)
cv2.imshow('Circle Over Tree Image', myimage)

cv2.circle(myimage, (140, 140), 36, (6, 233, 100), -1)
cv2.imshow('Masking the Image', myimage)
cv2.waitKey(0)
cv2.destroyAllWindows()
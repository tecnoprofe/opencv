import cv2
import os       # Importar la librería para manejo del Sistema Operativo

# Cargar el clasificador de rostro previamente entrenado
face_cascade = cv2.CascadeClassifier(os.path.dirname(__file__)+'\\haarcascade_frontalface_default.xml')

print("archivo: ",face_cascade)

# Inicializar la cámara web
cap = cv2.VideoCapture(0)

while True:
    # Leer un frame de la cámara
    ret, frame = cap.read()

    # Convertir el frame a escala de grises
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    if face_cascade.empty():
        raise IOError('No se pudo cargar el archivo XML del clasificador de Haar')
    # Detectar los rostros en el frame
    faces = face_cascade.detectMultiScale(gray, 1.1, 3)

    # Dibujar el cuadro delimitador alrededor de cada rostro detectado
    for (x, y, w, h) in faces:
        cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)

    # Mostrar el frame resultante en una ventana
    cv2.imshow('frame', frame)

    # Salir del bucle si se presiona la tecla 'q'
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Liberar la cámara y cerrar todas las ventanas
cap.release()
cv2.destroyAllWindows()
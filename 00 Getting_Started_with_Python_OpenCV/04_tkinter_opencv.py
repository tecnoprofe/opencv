from tkinter import*    
import tkinter as tk
import time    
import cv2
from PIL import Image
from PIL import ImageTk
import threading    

ws=Tk()

def button1_clicked(videoloop_stop):
    
    thread=threading.Thread(target=videoLoop, args=(videoloop_stop,)).start()

def button2_clicked(videloop_stop):
    #videoloop_stop[0] = True
    thread=threading.Thread(target=destroy, args=(panel,))

def destroy(panel):
    destroy()

def videoLoop(mirror=False):
    cap=cv2.VideoCapture(0)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)

    while True:
        ret, frame=cap.read()
        if mirror is True:
            frame=frame[:, ::-1]

        image=cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        image = Image.fromarray(image)
        image = ImageTk.PhotoImage(image)
        panel = tk.Label(image=image)
        panel.image = image
        panel.place(x=400, y=10)

        
        if videoloop_stop[0]:                       
            videoloop_stop[0] = False
            panel.destroy()
            break


videoloop_stop = [False]

button1 = tk.Button(ws, text="CAM ON", bg="green", font=("", 10),
    command=lambda: button1_clicked(videoloop_stop))
button1.place(x=300, y=20, width=70, height=30)

button2 = tk.Button(ws, text="CAM OFF", bg="red", font=("", 10),
    command=lambda: button2_clicked(videoloop_stop))
button2.place(x=300, y=50, width=70, height=30)

ws.mainloop()
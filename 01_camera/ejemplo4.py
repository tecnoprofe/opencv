import cv2
import numpy as np

cap = cv2.VideoCapture(0)

VerdeBajo1 = np.array([45, 50, 10], np.uint8)
VerdeAlto1 = np.array([70, 255, 255], np.uint8)

while True:
  ret,frame = cap.read()
  if ret==True:
    frameHSV = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    maskRed = cv2.inRange(frameHSV, VerdeBajo1, VerdeAlto1)
    maskRedvis = cv2.bitwise_and(frame, frame, mask= maskRed)        
    cv2.imshow('frame', frame)
    cv2.imshow('maskRed', maskRed)
    cv2.imshow('maskRedvis', maskRedvis)
    if cv2.waitKey(1) & 0xFF == ord('s'):
      break
cap.release()
cv2.destroyAllWindows()